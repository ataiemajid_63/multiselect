/**
 *
 * @param {Element} element
 * @param {Object} options
 * @returns {MultiSelect.refresh}
 *
 * options = {
 *      attrs: {
 *          attribute: value
 *      },
 *      direction: "rtl | ltr",
 *      onAddButtonClick: function(text, event){}
 * }
 */
function MultiSelect(element, options) {

    // Defined Elements
    var currentElement = $(element);
    var containerElement = $('<div>').addClass('form-control multi-select-container');
    var headElement = $('<a>').attr('tabindex', '0').addClass('form-control multi-select-head').html('<span class="labels"></span><div><i class="caret"></i></div>');
    var dropElement = $('<div>').addClass('multi-select-drop');
    var searchElement = $('<div>').addClass('multi-select-search');
    var inputSearchElement = $('<input type="text"/>').addClass('form-control input-sm');
    var resultsElement = $('<ul>').addClass('multi-select-results');
    var spaceCharPattern = new RegExp('[ ]*','g');

    // Defined Variables
    var items = [];
    var activeIndex = 0;

    // Defined Functions
    var init = function() {
        searchElement.html(inputSearchElement);
        searchElement.append('<div><i class="glyphicon glyphicon-search"></i></div>');
        dropElement.append(searchElement);
        dropElement.append(resultsElement);
        containerElement.append(headElement);
        containerElement.append(dropElement);

        currentElement.attr('multiple', 'multiple');

        if(options && options['attrs']) {
            var attrs = options['attrs'];
            for(attr in attrs) {
                if(attr === 'class') {
                    containerElement.addClass(attrs[attr]);
                    continue;
                }
                containerElement.attr(attr, attrs[attr]);
            }
        }

        containerElement.keydown(function(event) {
            if (event.keyCode === 27) {
                hide(function() {
                    headElement.focus();
                });
            }
        });

        if (options && options['direction'] && options['direction'] === 'rtl')
            containerElement.addClass('multi-select-rtl');

        inputSearchElement.focusout(function() {
            var isResultsFocus = dropElement.find('.multi-select-results:hover').length;
            if (dropElement.css('display') === 'block' && isResultsFocus === 0) {
                hide();
            }
        });

        inputSearchElement.keydown(function(event) {
            var liCount = resultsElement.children('li').length;
            var resultsElementHeight = $(resultsElement).height();
            var liHeight = resultsElement.children('li.active').outerHeight(true);
            switch (Number(event.keyCode)) {
                case 13:
                    if (activeIndex === 0) {
                        active(1);
                    }
                    toggleSelect(resultsElement.children('li.active'));
                    return false;
                case 40:
                    if (activeIndex >= liCount) {
                        active(1);
                    }
                    else {
                        active(activeIndex + 1);
                    }
                    resultsElement.scrollTop(liHeight * activeIndex - resultsElementHeight);
                    return false;
                case 38:
                    if (activeIndex <= 1) {
                        active(liCount);
                    }
                    else {
                        active(activeIndex - 1);
                    }
                    resultsElement.scrollTop(liHeight * activeIndex - resultsElementHeight);
                    return false;
            }
        });

        inputSearchElement.bind('input', function(event) {
            find($(this).val());
        });

        headElement.bind("click keydown", function(event) {
            if (event.keyCode === 32 || event.keyCode === 13 || event.type === 'click') {
                if (dropElement.css('display') === 'none') {
                    show();
                }
                else {
                    hide();
                }
            }
        });

        headElement.on('click', 'span.label .remove', function(event) {
            var optionIndex = $(this).closest('.label').data('remove');
            var lis = resultsElement.children('li');
            for (var i in lis) {
                if ($(lis[i]).data('optionIndex') === optionIndex) {
                    toggleSelect(lis[i]);
                    break;
                }
            }

            return false;
        });

        resultsElement.on('click', 'li', function() {
            toggleSelect(this);
            inputSearchElement.focus();
        });

        refresh();

        //Hidden Select element and insert multi select component
        currentElement.css('display', 'none');
        currentElement.after(containerElement);
    };

    var collect = function() {
        var exist;
        var i;
        currentElement.children('option').each(function(number, element) {
            exist = false;
            for (i = 0; i < items.length; i++) {
                if (items[i].caption === $(element).html()) {
                    exist = true;
                    break;
                }
            }

            if (!exist && $(element).html() !== "") {
                items.push({
                    optionIndex: number + 1,
                    caption: $(element).html(),
                    value: $(element).val(),
                    option: element,
                    selected: $(element).attr('selected') === undefined ? false : true
                });
            }
        });
    };

    var find = function(text) {
        resultsElement.empty();

        if (!text) {
            for (var i in items) {
                var li = $('<li>');
                li.html(items[i].caption);
                li.data('option-index', items[i].optionIndex);
                if (items[i].selected) {
                    li.addClass('selected');
                }
                resultsElement.append(li);
            }
            return;
        }

        items.forEach(function(item) {
            if (new RegExp(escape(text.toLowerCase())).test(escape(item.caption.toLowerCase()))) {
                var li = $('<li>');
                li.html(item.caption);
                li.data('option-index', item.optionIndex);
                if (item.selected) {
                    li.addClass('selected');
                }
                resultsElement.append(li);
            }
        });

        if (resultsElement.children().length === 0) {
            var li = $('<li>');
            var message = 'گزینه ای با عنوان <b>' + text + '</b> یافت نشد. ';
            li.html(message);
            if (options['onAddButtonClick']) {
                var addButton = $('<button type="button">').addClass('btn btn-xs btn-info').text('افزودن به لیست').click(function(event) {
                    options['onAddButtonClick'](text, event);
                });
                li.append(addButton);
            }
            resultsElement.append(li);
        }

        active(1);
    };

    var show = function() {
        if(currentElement.hasClass('multiselect-dropdown')) {
            dropElement.css('top', containerElement.height());
        }
        else if(currentElement.hasClass('multiselect-dropup')) {
            dropElement.css('bottom', containerElement.height() + 2);
        }
        else if(currentElement.hasClass('multiselect-dropleft')) {
            dropElement.css('right', containerElement.width() + 2);
            dropElement.css('top', -2);
        }
        else if(currentElement.hasClass('multiselect-dropright')) {
            dropElement.css('left', containerElement.width() + 2);
            dropElement.css('top', -2);
        }

        dropElement.fadeIn('fast', function() {
            inputSearchElement.focus();
            find(inputSearchElement.val());
        });
    };

    var hide = function(callback) {
        dropElement.fadeOut('fast', callback);
        activeIndex = 0;
    };

    var toggleSelect = function(selectedElement) {
        var item;
        var $labelElement;
        var $removeElement = $('<i>').addClass('glyphicon glyphicon-remove remove');
        var $selectedElement = $(selectedElement);
        for (var i = 0; i < items.length; i++) {
            $labelElement = $('<span>').addClass('label label-default');
            item = items[i];
            if (item.optionIndex === Number($selectedElement.data('option-index'))) {
                if (item.selected) {
                    headElement.find('span.val-' + item.value.replace(spaceCharPattern, '')).remove();
                }
                else {
                    $labelElement.data('remove', item.optionIndex);
                    headElement.find('> span.labels').append($labelElement.addClass('val-' + item.value.replace(spaceCharPattern, '')).html(" " + item.caption + " ").append($removeElement));
                }

                $selectedElement.toggleClass('selected');
                item.option.selected = !item.option.selected;
                item.selected = !item.selected;
                currentElement.change();
                break;
            }
        }
    };

    var active = function(index) {
        if (index)
            activeIndex = index;

        resultsElement.children('li.active').removeClass('active');
        resultsElement.children('li:nth-child(' + activeIndex + ')').addClass('active');
    };

    var refresh = function() {
        collect();

        headElement.children('span.labels').html('&nbsp;');

        var item;
        var $labelElement;
        var $removeElement;
        for (var i = 0; i < items.length; i++) {
            $labelElement = $('<span>').addClass('label label-default');
            $removeElement = $('<i>').addClass('glyphicon glyphicon-remove remove');
            item = items[i];
            if (item.selected) {
                $labelElement.data('remove', item.optionIndex);
                headElement.find('> span.labels').append($labelElement.addClass('val-' + item.value.replace(spaceCharPattern, '')).html(" " + item.caption + " ").append($removeElement));
            }
        }

        find();
    };

    var clear = function() {
        for(var i = 0; i < items.length; i++) {
            if(items[i].selected) {
                items[i].selected = false;
                items[i].option.selected = false;
                headElement.find('span.val-' + items[i].value.replace(spaceCharPattern, '')).remove();
                currentElement.change();
            }
        }
    };

    init();

    return {
        refresh: function() {
            refresh();
            find(inputSearchElement.val());
            inputSearchElement.focus();
        },
        clear: clear
    };
}

//jQuery Plugins
/**
 *
 * @param {Object} options
 * @returns {$.fn@call;each}
 */
$.fn.multiSelect = function(options) {
    return this.each(function() {
        var $this = $(this);
        var multiSelect = $this.data('multiSelect');
        if (!multiSelect) {
            $this.data('multiSelect', (multiSelect = MultiSelect(this, options)));
        }
        if (typeof options === 'string') {
            multiSelect[options]();
        }
    });
};
